# Fork Ts Checker Webpack Plugin Limiter

Limit the active forks to prevent EMFILE error when working with large number of entries.

## CAVEAT

This plugin is inspired by [TypeStrong/fork-ts-checker-webpack-plugin#424](https://github.com/TypeStrong/fork-ts-checker-webpack-plugin/issues/424)
and depends on alpha features at the moment of authoring, please use at your own
risk.

## Usage

In your `webpack.config.js`, add this plugin along with `fork-ts-checker-webpack-plugin`.

The only option is `concurrency` which limits the active forks allowed in the plugin, defaults to the number of CPUs in
your workstation.

```javascript
const ForkTsCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin");
const ForkTsCheckerWebpackPluginLimiter = require("fork-ts-checker-webpack-plugin-limiter");

module.exports = {
  plugins: [
    new ForkTsCheckerWebpackPlugin(),
    new ForkTsCheckerWebpackPluginLimiter({ concurrency: 5 }),
  ],
  ...
  use: {
    loader: "ts-loader",
    options: {
      transpileOnly: true // for the plugin
    },
  },
  ...
};
```
