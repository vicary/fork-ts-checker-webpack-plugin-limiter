const ForkTsCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin");
const pLimit = require("p-limit");
const os = require("os");

class ForkTsCheckerWebpackPluginLimiter {
  constructor({ concurrency = Math.min(1, os.cpus().length - 1) } = {}) {
    this.limiter = pLimit(concurrency);
  }

  apply(compiler) {
    ForkTsCheckerWebpackPlugin
      .getCompilerHooks(compiler)
      .start
      .tapPromise(
        "ForkTsCheckerWebpackPluginLimiter", 
        () => this.limiter((changes) => changes)
      );
  }
}

module.exports = ForkTsCheckerWebpackPluginLimiter;
